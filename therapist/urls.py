from django.conf.urls import patterns, include, url
from . import views

urlpatterns = patterns('',
    url(r'^$', views.home),
    url(r'^patients$', views.patient_list),
    url(r'^patients/(?P<pk>[0-9]+)/$', views.patient),
    url(r'^info$', views.info),
    url(r'^patients/[0-9]/ex/(?P<pk>[0-9]+)$', views.exercise),

    url(r'^api/get/(?P<pk>[0-9]+).json', views.get),
    url(r'^api/put/(?P<pk>[0-9]+).json', views.put),
    url(r'^api/post_template.json', views.post_template),
)
