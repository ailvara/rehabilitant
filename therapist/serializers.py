from django.forms import widgets
from rest_framework import serializers
from .models import Exercise, Points

class PointsSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    points = serializers.CharField()

    def create(self, validated_data):
        return Points.objects.create(**validated_data)

    def update(self, instance, validated_data):
        return instance

class ExerciseSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    date_added = serializers.DateTimeField()
    scheduled_for = serializers.DateField()
    date_done = serializers.DateTimeField(required=False)
    points_given = PointsSerializer()
    points_done = serializers.CharField(required=False)
    step = serializers.FloatField(default=0.1)

    def create(self, validated_data):
        return Exercise.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.date_done = validated_data.get('date', instance.date_done)
        instance.points_done = validated_data.get('points_done', instance.points_done)
        instance.save()
        return instance
