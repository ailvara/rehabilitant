from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Patient)
admin.site.register(Exercise)
admin.site.register(Comment)
admin.site.register(Points)
