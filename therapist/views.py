from django.shortcuts import render
from .models import Patient, Exercise, Comment, Points
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.utils import timezone
from datetime import date

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from .serializers import ExerciseSerializer

from rest_framework.decorators import api_view
from rest_framework.response import Response

# Create your views here.
def home(request):
    return render(request, 'home.html', {})

def patient_list(request):
    patients = Patient.objects.filter().order_by('last_name')
    return render(request, 'patient_list.html', {'patients': patients})

def patient(request, pk):
    patient = Patient.objects.get(pk=pk)
    exercises = Exercise.objects.filter(patient=patient).order_by('date_done')

    labels = tuple()
    avgs = tuple()
    maxs = tuple()
    for ex in exercises:
        if ex.date_done is not None:
            avgs = avgs + (ex.avg(),)
            maxs = maxs + (ex.maxval(),)
            labels = labels + (ex.date_done.strftime('%y-%m-%d'),)

    slabels = str(labels).replace("'","")
    comments = Comment.objects.filter(patient=patient)
    return render(request, 'patient.html', {
        'patient': patient,
        'exercises': exercises,
        'comments': comments,
        'avgs': str(avgs)[1:-1],
        'maxs': str(maxs)[1:-1],
        'labels': str(labels)[1:-1]
    })

def exercise(request, pk):
    exercise = Exercise.objects.get(pk=pk)
    if exercise is not None:
        return render(request, 'exercise.html', {'ex': exercise})
    else:
        return render(request, 'patient.html', {})

def info(request):
    return render(request, 'info.html', {})

def get(request, pk, format='json'):
    patient = Patient.objects.get(pk=pk)
    exs = Exercise.objects.filter(patient=patient)
    serializer = ExerciseSerializer(exs, many=True)
    return HttpResponse(serializer.data, content_type='application/json')

def put(request, pk):
    ex = Exercise.objects.get(pk=pk)
    date_done = request.GET.get('date_done', '')
    points_done = request.GET.get('points_done', '[]')
    setattr(ex, 'date_done', date_done)
    setattr(ex, 'points_done', points_done)
    ex.save()
    return HttpResponse('OK')

def post_template(request):
    new_points = request.GET.get('points','[]')
    author_id = request.GET.get('author',1)
    name = request.GET.get('name','')
    author = User.objects.get(pk=author_id)
    step = request.GET.get('step',0.1)
    new_defined = Points.objects.create(author=author, points=new_points, name=name, step=step)
    return HttpResponse('OK')
