from django.db import models
from django.utils import timezone
import statistics

class Patient(models.Model):
    therapist = models.ForeignKey('auth.User')
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    date_added = models.DateTimeField(default=timezone.now)

    def publish(self):
        self.save()

    def __str__(self):
        return self.first_name + " " + self.last_name

class Points(models.Model):
    author = models.ForeignKey('auth.User')
    name = models.CharField(max_length=50)
    points = models.TextField(default="")
    step = models.FloatField(default=0.1)

    def publish(self):
        self.save()

    def __str__(self):
        return self.name

class Exercise(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    scheduled_for = models.DateField()
    date_done = models.DateTimeField(null=True, blank=True)

    patient = models.ForeignKey('Patient')
    comment = models.CharField(max_length=200, default="")

    points_given = models.ForeignKey('Points')
    points_done = models.TextField(null=True, blank=True)
    step = models.FloatField(default=0.1)

    def publish(self):
        self.save()

    #niesprawdzone
    def mark_done(self, points_done, date_done):
        self.points_done = points_done
        self.date_done = date_done
        self.save()

    def __str__(self):
        return str(self.patient) + ": " + str(self.scheduled_for) + "/" + str(self.date_done)

    def avg(self):
        prep = self.points_done[1:-1].split(',')
        list = tuple()
        for item in prep:
            if item.isdigit() and len(item)>0:
                list = list + (float(item),)
        if len(list) > 0:
            avg = statistics.mean(list)
        else:
            avg = 0
        return avg

    def maxval(self):
        prep = self.points_done[1:-1].split(',')
        list = tuple()
        for item in prep:
            if item.isdigit() and len(item)>0:
                list = list + (float(item),)
        if len(list) > 0:
            maxval = max(list)
        else:
            maxval = 0
        return maxval

class Comment(models.Model):
    author = models.ForeignKey('auth.User')
    patient = models.ForeignKey('Patient')
    date_added = models.DateTimeField(default=timezone.now)
    content = models.TextField(default="")

    def publish(self):
        self.save()

    def __str__(self):
        return str(self.patient) + " (" + str(self.date_added) +")"
