# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('therapist', '0005_auto_20150410_2031'),
    ]

    operations = [
        migrations.AddField(
            model_name='exercise',
            name='comment',
            field=models.CharField(max_length=200, default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='exercise',
            name='date_done',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=True,
        ),
    ]
