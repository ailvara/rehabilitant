# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('therapist', '0013_auto_20150428_2222'),
    ]

    operations = [
        migrations.AddField(
            model_name='points',
            name='step',
            field=models.FloatField(default=0.1),
            preserve_default=True,
        ),
    ]
