# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('therapist', '0003_auto_20150326_1650'),
    ]

    operations = [
        migrations.AddField(
            model_name='exercise',
            name='scheduled_for',
            field=models.DateField(default=datetime.datetime(2015, 3, 26, 16, 52, 28, 710087)),
            preserve_default=False,
        ),
    ]
