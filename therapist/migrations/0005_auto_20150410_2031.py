# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('therapist', '0004_exercise_scheduled_for'),
    ]

    operations = [
        migrations.AddField(
            model_name='exercise',
            name='points_done',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='exercise',
            name='points_given',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='exercise',
            name='step',
            field=models.FloatField(default=0.1),
            preserve_default=True,
        ),
    ]
