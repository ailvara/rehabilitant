# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('therapist', '0009_auto_20150411_1546'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exercise',
            name='date_done',
            field=models.DateTimeField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
