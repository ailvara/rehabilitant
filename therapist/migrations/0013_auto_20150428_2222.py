# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('therapist', '0012_points'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exercise',
            name='points_given',
            field=models.ForeignKey(to='therapist.Points'),
            preserve_default=True,
        ),
    ]
