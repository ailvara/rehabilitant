# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('therapist', '0010_auto_20150411_1547'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exercise',
            name='points_done',
            field=models.TextField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
