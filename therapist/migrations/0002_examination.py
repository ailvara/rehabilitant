# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('therapist', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Examination',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('date_added', models.DateTimeField(default=django.utils.timezone.now)),
                ('patient', models.ForeignKey(to='therapist.Patient')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
