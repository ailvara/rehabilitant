# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('therapist', '0006_auto_20150410_2140'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exercise',
            name='date_done',
            field=models.DateTimeField(blank=True),
            preserve_default=True,
        ),
    ]
