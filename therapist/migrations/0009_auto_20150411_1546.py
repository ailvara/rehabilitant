# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('therapist', '0008_comment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exercise',
            name='date_done',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]
