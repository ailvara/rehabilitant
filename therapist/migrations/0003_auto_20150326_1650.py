# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('therapist', '0002_examination'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Examination',
            new_name='Exercise',
        ),
    ]
